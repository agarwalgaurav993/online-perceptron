'''
Created on 19 Feb, 2017

@author: gaurav
'''
import pylab as pl
import numpy as np
import DataManipulation as dm

figHandle = "Online Perceptron"
xlabel = "Feature X1"
ylabel = "Feature X2"

sSize = 1
def classifier12(X, Y):
    w = np.zeros((1,3))
    i = 0
    iterations = 0
    while i < len(X):
        x = X[i]
        fx = np.dot(w, x)
        if fx*Y[i] <= 0:
            w = w + sSize*Y[i]*x
            i = 0
            iterations += 1
            continue
        i = i + 1
    print w
    print "Number of Iterations = ", iterations
    return w[0, :]

def main():
    cd1 = dm.C2
    cd2 = dm.C3
    X, Y = dm.augment_dataSet(cd1, cd2)
    abc = classifier12(X, Y)
    dm.pointsPlotting(cd1, "red", figHandle, xlabel, ylabel)
    dm.pointsPlotting(cd2, "blue", figHandle, xlabel, ylabel)
    x, y = dm.getpointsOnLine(abc)
    dm.linePlotting(x, y, figHandle, xlabel, ylabel, l = "Perceptron Classifier")
    pl.legend()
    pl.show()

if __name__ == '__main__':
    main()