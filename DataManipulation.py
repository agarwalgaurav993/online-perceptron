'''
Created on 23 Feb, 2017

@author: gaurav
'''
import numpy as np
import pylab as pl
C1 = np.array([(0.1, 1.1), (6.8, 7.1), (-3.5, -4.1), (2.0, 2.7), (4.1, 2.8),
               (3.1, 5.0), (-0.8, -1.3), (0.9, 1.2), (5.0, 6.4), (3.9, 4.0)], float)
C2 = np.array([(7.1, 4.2), (-1.4, -4.3), (4.5, 0.0), (6.3, 1.6), (4.2, 1.9),
               (1.4, -3.2), (2.4, -4.0), (2.5, -6.1), (8.4, 3.7), (4.1, -2.2)], float)
C3 = np.array([(-3.0,-2.9), (0.5,8.7), (2.9,2.1), (-0.1,5.2), (-4.0,2.2),
               (-1.3,3.7), (-3.4,6.2), (-4.1,3.4), (-5.1,1.6), (1.9,5.1)], float)

def augment_dataSet(X1, X2):
    trainSet = np.vstack((X1,X2))
    Y = np.vstack((np.ones((len(X1), 1)), -np.ones((len(X2), 1))))
    bias = np.ones((len(trainSet), 1))
    trainSet = np.hstack((trainSet, bias))
    return trainSet, Y

def getpointsOnLine(abc):
    a, b, c = abc
    slope, intercept = -(a/b), -(c/b)
    x1, y1 = 8.0, slope*8.0 + intercept
    x2, y2 = -4.0, slope*-4.0 + intercept
    return [x1, x2], [y1, y2]

def pointsPlotting(dataSet, col, figHandle, xlabel, ylabel):
    #Plotting Dataset
    pl.figure(figHandle)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    ax = pl.subplot(111)
    ax.scatter(dataSet[:, 0], dataSet[:,1], c=col)

def linePlotting(x, y, figHandle, xlabel, ylabel, l):
    pl.figure(figHandle)
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    ax = pl.subplot(111)
    ax.plot(x, y, label=l)
